package com.atlassian.aui.soy;

import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class isListFunction implements SoyServerFunction<Boolean>, SoyClientFunction {

    public String getName() {
        return "isList";
    }

    public JsExpression generate(JsExpression... jsExpressions) {
        return new JsExpression("(" + jsExpressions[0].getText() + ") instanceof Array");
    }

    public Boolean apply(Object... objects) {
        return objects[0] instanceof Iterable;
    }

    public Set<Integer> validArgSizes() {
        return ImmutableSet.of(1);
    }
}
