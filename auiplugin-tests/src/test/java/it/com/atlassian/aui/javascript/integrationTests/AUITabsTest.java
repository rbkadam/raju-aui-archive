package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.webdriver.utils.JavaScriptUtils;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.TabsTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AUITabsTest extends AbstractAuiIntegrationTest
{
    private TabsTestPage tabsTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setup()
    {
        tabsTestPage = product.visit(TabsTestPage.class);
        elementFinder = tabsTestPage.getElementFinder();
    }

    // First pane of tabs should  be visible by default
    @Test
    public void testAUITabsFirstPaneVisibleByDefault()
    {
        PageElement horizontalTabs = elementFinder.find(By.id("horizontal-first"));
        PageElement verticalTabs = elementFinder.find(By.id("vertical-fifth"));

        assertTrue(horizontalTabs.isPresent());
        assertTrue(horizontalTabs.isVisible());
        assertTrue(verticalTabs.isPresent());
        assertTrue(verticalTabs.isVisible());
    }

    // Only one pane should be visible
    @Test
    public void testAUITabsOnlyOnePaneVisible()
    {
        PageElement horizontalFirst = elementFinder.find(By.id("horizontal-first"));
        PageElement horizontalSecond = elementFinder.find(By.id("horizontal-second"));
        PageElement horizontalThird = elementFinder.find(By.id("horizontal-third"));
        PageElement horizontalFourth = elementFinder.find(By.id("horizontal-fourth"));

        PageElement verticalFifth = elementFinder.find(By.id("vertical-fifth"));
        PageElement verticalSixth = elementFinder.find(By.id("vertical-sixth"));
        PageElement verticalSeventh = elementFinder.find(By.id("vertical-seventh"));
        PageElement verticalEighth = elementFinder.find(By.id("vertical-eighth"));

        assertTrue(horizontalFirst.isVisible());
        assertTrue(Conditions.not(horizontalSecond.timed().isVisible()).now());
        assertTrue(Conditions.not(horizontalThird.timed().isVisible()).now());
        assertTrue(Conditions.not(horizontalFourth.timed().isVisible()).now());

        assertTrue(verticalFifth.isVisible());

        assertTrue(Conditions.not(verticalSixth.timed().isVisible()).now());
        assertTrue(Conditions.not(verticalSeventh.timed().isVisible()).now());
        assertTrue(Conditions.not(verticalEighth.timed().isVisible()).now());
    }

    // The visible pane should be the active-pane
    @Test
    public void testAUITabsVisiblePaneIsActive()
    {

        PageElement horizontalFirst = elementFinder.find(By.id("horizontal-first"));
        PageElement horizontalSecond = elementFinder.find(By.id("horizontal-second"));
        PageElement horizontalThird = elementFinder.find(By.id("horizontal-third"));
        PageElement horizontalFourth = elementFinder.find(By.id("horizontal-fourth"));

        PageElement verticalFifth = elementFinder.find(By.id("vertical-fifth"));
        PageElement verticalSixth = elementFinder.find(By.id("vertical-sixth"));
        PageElement verticalSeventh = elementFinder.find(By.id("vertical-seventh"));
        PageElement verticalEighth = elementFinder.find(By.id("vertical-eighth"));

        assertTrue(horizontalFirst.isVisible());
        assertTrue(horizontalFirst.hasClass("active-pane"));

        assertFalse(horizontalSecond.hasClass("active-pane"));
        assertFalse(horizontalThird.hasClass("active-pane"));
        assertFalse(horizontalFourth.hasClass("active-pane"));

        assertTrue(verticalFifth.isVisible());
        assertTrue(verticalFifth.hasClass("active-pane"));

        assertFalse(verticalSixth.hasClass("active-pane"));
        assertFalse(verticalSeventh.hasClass("active-pane"));
        assertFalse(verticalEighth.hasClass("active-pane"));
    }

    // First  menu item should be active by default
    public void testAUITabsFirstMenuItemActiveByDefault()
    {
        PageElement horizontal = elementFinder.find(By.cssSelector("#horizontal li:first-child"));
        PageElement vertical = elementFinder.find(By.cssSelector("#vertical li:first-child"));

        assertTrue(horizontal.hasClass("active-tab"));
        assertTrue(vertical.hasClass("active-tab"));
    }

    // Only one menu item should be active
    @Test
    public void testAUITabsOnlyOneMenuItemActive()
    {
        PageElement horizontalOne = elementFinder.find(By.cssSelector("#horizontal ul.tabs-menu li.menu-item:nth-child(1)"));
        PageElement horizontalTwo = elementFinder.find(By.cssSelector("#horizontal ul.tabs-menu li.menu-item:nth-child(2)"));
        PageElement horizontalThree = elementFinder.find(By.cssSelector("#horizontal ul.tabs-menu li.menu-item:nth-child(3)"));
        PageElement horizontalFour = elementFinder.find(By.cssSelector("#horizontal ul.tabs-menu li.menu-item:nth-child(4)"));

        PageElement verticalOne = elementFinder.find(By.cssSelector("#vertical ul.tabs-menu li.menu-item:nth-child(1)"));
        PageElement verticalTwo = elementFinder.find(By.cssSelector("#vertical ul.tabs-menu li.menu-item:nth-child(2)"));
        PageElement verticalThree = elementFinder.find(By.cssSelector("#vertical ul.tabs-menu li.menu-item:nth-child(3)"));
        PageElement verticalFour = elementFinder.find(By.cssSelector("#vertical ul.tabs-menu li.menu-item:nth-child(4)"));

        assertTrue(horizontalOne.hasClass("active-tab"));
        assertFalse(horizontalTwo.hasClass("active-tab"));
        assertFalse(horizontalThree.hasClass("active-tab"));
        assertFalse(horizontalFour.hasClass("active-tab"));

        assertTrue(verticalOne.hasClass("active-tab"));
        assertFalse(verticalTwo.hasClass("active-tab"));
        assertFalse(verticalThree.hasClass("active-tab"));
        assertFalse(verticalFour.hasClass("active-tab"));
    }

    // clicking menu item should show associated pane and hide all others
    @Test
    public void testAUITabsClickingMenuItemShouldShowAssociatedPane()
    {
        PageElement link = elementFinder.find(By.cssSelector("#horizontal ul.tabs-menu .menu-item:nth-child(2) > a"));
        PageElement horizontalFirst = elementFinder.find(By.id("horizontal-first"));
        PageElement horizontalSecond = elementFinder.find(By.id("horizontal-second"));
        PageElement horizontalThird = elementFinder.find(By.id("horizontal-third"));
        PageElement horizontalFourth = elementFinder.find(By.id("horizontal-fourth"));

        link.click();

        assertTrue(horizontalSecond.isVisible());
        assertTrue(Conditions.not(horizontalFirst.timed().isVisible()).now());
        assertTrue(Conditions.not(horizontalThird.timed().isVisible()).now());
        assertTrue(Conditions.not(horizontalFourth.timed().isVisible()).now());
    }

    // test that nested tabs work (basic event test)
    @Test
    public void testNestedAUITabs()
    {

        PageElement outerHorizontalTab = elementFinder.find(By.id("nested-tabs-horizontal-outer-horizontal"));
        PageElement innerHorizontalTab = elementFinder.find(By.id("tabs-nested-example1-inner-horizontal-second-trigger"));

        outerHorizontalTab.click();
        innerHorizontalTab.click();

        PageElement outerFirst = elementFinder.find(By.id("tabs-nested-example1-outer-first"));
        PageElement outerSecond = elementFinder.find(By.id("tabs-nested-example1-outer-second"));

        PageElement outerHorizontalThird = elementFinder.find(By.id("tabs-nested-example1-outer-third"));
        PageElement outerHorizontalFourth = elementFinder.find(By.id("tabs-nested-example1-outer-fourth"));

        // check the correct outer tab is visible
        assertFalse(outerFirst.isVisible());
        assertTrue(outerSecond.isVisible());
        assertFalse(outerHorizontalThird.isVisible());
        assertFalse(outerHorizontalFourth.isVisible());

        PageElement innerHorizontalFirst = elementFinder.find(By.id("tabs-nested-example1-inner-horizontal-first"));
        PageElement innerHorizontalSecond = elementFinder.find(By.id("tabs-nested-example1-inner-horizontal-second"));
        PageElement innerHorizontalThird = elementFinder.find(By.id("tabs-nested-example1-inner-horizontal-third"));
        PageElement innerHorizontalFourth = elementFinder.find(By.id("tabs-nested-example1-inner-horizontal-fourth"));

        // check the corrent nested tab is visible
        assertFalse(innerHorizontalFirst.isVisible());
        assertTrue(innerHorizontalSecond.isVisible());
        assertFalse(innerHorizontalThird.isVisible());
        assertFalse(innerHorizontalFourth.isVisible());

        PageElement horizontalOuterVertical = elementFinder.find(By.id("nested-tabs-horizontal-outer-vertical"));
        PageElement nestedInnerSecondTrigger = elementFinder.find(By.id("tabs-nested-example1-inner-second-trigger"));

        horizontalOuterVertical.click();
        nestedInnerSecondTrigger.click();

        // check the correct outer tab is visible
        assertTrue(outerFirst.isVisible());
        assertFalse(outerSecond.isVisible());
        assertFalse(outerHorizontalThird.isVisible());
        assertFalse(outerHorizontalFourth.isVisible());

        PageElement nestedInnerFirst = elementFinder.find(By.id("tabs-nested-example1-inner-first"));
        PageElement nestedInnerSecond = elementFinder.find(By.id("tabs-nested-example1-inner-second"));
        PageElement nestedInnerThird = elementFinder.find(By.id("tabs-nested-example1-inner-third"));
        PageElement nestedInnerFourth = elementFinder.find(By.id("tabs-nested-example1-inner-fourth"));

        // check the corrent nested tab is visible
        assertFalse(nestedInnerFirst.isVisible());
        assertTrue(nestedInnerSecond.isVisible());
        assertFalse(nestedInnerThird.isVisible());
        assertFalse(nestedInnerFourth.isVisible());
    }

    @Test
    public void testAddingTitlesToVerticalAUITabs()
    {
        PageElement verticalSixthTrigger = elementFinder.find(By.id("vertical-titles-sixth-trigger"));
        PageElement verticalSeventhTrigger = elementFinder.find(By.id("vertical-titles-seventh-trigger"));
        WebDriverElement verticalEighthTrigger = (WebDriverElement) elementFinder.find(By.id("vertical-titles-eighth-trigger"));

        // title comes from HTML - JS should skip this one
        assertTrue(verticalSixthTrigger.hasAttribute("title", "This title should not be overridden with the visible text"));

        // title added with JS - should take visible tab text and set as title
        assertTrue(verticalSeventhTrigger.hasAttribute("title", "Tab 3 has a very long tab name"));

        // title should not be added to short tab controls
        // check the link is present but does NOT have a title attribute
        assertTrue(verticalEighthTrigger.isPresent());

        String attrValue = JavaScriptUtils.execute(String.class, "return arguments[0].getAttribute('title')",
                product.getTester().getDriver(), verticalEighthTrigger.asWebElement());

        assertEquals("title attribute should not be present", null,  attrValue);
    }
}
