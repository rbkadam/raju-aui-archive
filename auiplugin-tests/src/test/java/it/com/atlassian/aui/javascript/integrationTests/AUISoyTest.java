package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.SoyTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @since 4.0
 */
public class AUISoyTest extends AbstractAuiIntegrationTest
{

    private SoyTestPage soyTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setup()
    {
        soyTestPage = product.visit(SoyTestPage.class);
        elementFinder = soyTestPage.getElementFinder();
    }
    
    private void assertHasClass(PageElement el, String cssClass) {
        assertTrue("Should have class '" + cssClass + "'", el.hasClass(cssClass));
    }
    
    private void assertTextEquals(PageElement el, String text) {
        assertEquals("Inner text should be '" + text + "'", text, el.getText());
    }
    
    private void assertAttributeEquals(PageElement el, String attr, String expectedValue) {
        String val = el.getAttribute(attr);
        assertEquals(attr + " should be '" + expectedValue + "'", expectedValue, val);
    }

    private void assertHashEquals(PageElement el, String attr, String hash) {
        String attrVal = el.getAttribute(attr);
        assertEquals(attr + " should be '" + hash + "'", hash, attrVal.substring(attrVal.indexOf("#")));
    }

    private void assertHashEquals(PageElement el, String hash) {
        assertHashEquals(el, "href", hash);
    }

    private void assertHasRootElementProperties(PageElement element, String expectedId, String expectedTagName)
    {
        assertTrue("Element should exist", element.isPresent());
        if (expectedId != null) {
            assertEquals("Root element should have id '" + expectedId + "'.", expectedId, element.getAttribute("id"));
        }
        
        assertHasClass(element, "extra-class");
        assertEquals("Root element should have attribute 'data-attr' set to 'extra-attr'", "extra-attr", element.getAttribute("data-attr"));
        
        if (expectedTagName != null) {
            assertEquals("Root element should have tagName '" + expectedTagName + "'.", expectedTagName, element.getTagName());
        }
    }
    
    @Test
    public void testComplexPanel()
    {
        PageElement complexPanel = elementFinder.find(By.id("soy-panel"));
        assertHasRootElementProperties(complexPanel, "soy-panel", "section");
        assertTextEquals(complexPanel, "AUI Panel &");
    }

    @Test
    public void testComplexGroup()
    {
        PageElement complexGroup = elementFinder.find(By.id("soy-group"));
        assertHasRootElementProperties(complexGroup, "soy-group", "section");
        assertHasClass(complexGroup, "aui-group-split");
        
        PageElement complexItem1 = elementFinder.find(By.id("soy-item-1"));
        assertHasRootElementProperties(complexItem1, "soy-item-1", "section");
        assertTextEquals(complexItem1, "AUI Item 1 in AUI Group &");
    }

    @Test
    public void testComplexTable()
    {
        PageElement complexTable = elementFinder.find(By.id("soy-table"));
        assertHasRootElementProperties(complexTable, "soy-table", null);

        PageElement column = complexTable.find(By.id("complex-table-column"));
        assertEquals("Specified column is present", "col", column.getTagName());

        PageElement tbody = complexTable.find(By.cssSelector("tbody"));
        assertTextEquals(tbody, "AUI Table (tbody) &");

        PageElement thead = complexTable.find(By.cssSelector("thead"));
        assertTextEquals(thead, "AUI Table (thead) &");

        PageElement tfoot = complexTable.find(By.cssSelector("tfoot"));
        assertTextEquals(tfoot, "AUI Table (tfoot) &");

        PageElement caption = complexTable.find(By.cssSelector("caption"));
        assertTextEquals(caption, "AUI Table (caption) &");
    }

    @Test
    public void testComplexTabs()
    {
        PageElement complexTabs = elementFinder.find(By.id("soy-tabs"));
        assertHasRootElementProperties(complexTabs, "soy-tabs", "section");
        assertHasClass(complexTabs, "vertical-tabs");
        assertHasClass(complexTabs, "aui-tabs-disabled");

        PageElement tabMenuItem1 = complexTabs.find(By.id("soy-tab-menu-1"));
        assertHasRootElementProperties(tabMenuItem1, "soy-tab-menu-1", null);
        assertHasClass(tabMenuItem1, "active-tab");
        assertHashEquals(tabMenuItem1.find(By.tagName("a")), "#soy-tab-pane-1");
        assertTextEquals(tabMenuItem1, "Tab 1 &amp;");

        PageElement tabPane1 = complexTabs.find(By.id("soy-tab-pane-1"));
        assertHasRootElementProperties(tabPane1, "soy-tab-pane-1", "section");
        assertHasClass(tabPane1, "active-pane");
        assertTextEquals(tabPane1, "Tab 1 Content - Disabled tabs &");
    }

    @Test
    public void testComplexDropdown()
    {
        PageElement complexDropdownParent = elementFinder.find(By.id("soy-dropdown-parent"));
        assertHasRootElementProperties(complexDropdownParent, "soy-dropdown-parent", "section");

        PageElement trigger = complexDropdownParent.find(By.id("soy-dropdown-trigger"));
        assertHasRootElementProperties(trigger, "soy-dropdown-trigger", null);
        assertTextEquals(trigger, "AUI Dropdown Trigger &amp;");
        
        PageElement triggerIcon = trigger.find(By.cssSelector(".icon"));
        assertTrue("Should not have an icon", !triggerIcon.isPresent());
        
        trigger.click();

        PageElement menu = complexDropdownParent.find(By.id("soy-dropdown-menu"));
        assertHasRootElementProperties(menu, "soy-dropdown-menu", "section");
        
        PageElement dropdownItem = menu.find(By.id("soy-dropdown-item"));
        assertHasRootElementProperties(dropdownItem, "soy-dropdown-item", "section");
        assertTextEquals(dropdownItem, "AUI Dropdown Item &amp;");
        assertHashEquals(dropdownItem.find(By.tagName("a")), "#dropdown-item");
    }

    @Test
    public void testComplexToolbar()
    {
        PageElement complexToolbar = elementFinder.find(By.id("soy-toolbar"));
        assertHasRootElementProperties(complexToolbar, "soy-toolbar", "section");

        PageElement splitLeft = complexToolbar.find(By.id("soy-toolbar-split-left"));
        assertHasRootElementProperties(splitLeft, "soy-toolbar-split-left", "section");
        assertHasClass(splitLeft, "toolbar-split-left");

        PageElement group = splitLeft.find(By.id("soy-toolbar-group"));
        assertHasRootElementProperties(group, "soy-toolbar-group", null);

        //Button 1

        PageElement button1 = group.find(By.id("soy-toolbar-button-1"));
        assertHasRootElementProperties(button1, "soy-toolbar-button-1", null);
        assertHasClass(button1, "active");
        assertHasClass(button1, "primary");

        PageElement button1Trigger = button1.find(By.className("toolbar-trigger"));
        assertHashEquals(button1Trigger, "#toolbar-button");
        assertTextEquals(button1Trigger, "AUI Button (text) &amp;");

        //Button 2

        PageElement button2 = group.find(By.id("soy-toolbar-button-2"));
        PageElement button2icon = button2.find(By.cssSelector(".toolbar-trigger .icon"));
        assertHasClass(button2icon, "my-toolbar-icon");

        //Link

        PageElement link = group.find(By.id("soy-toolbar-link"));
        assertHasRootElementProperties(link, "soy-toolbar-link", null);
        assertHashEquals(link.find(By.className("toolbar-trigger")), "#link");
        assertTextEquals(link, "AUI Link &amp;");

        //Dropdown

        PageElement dropdown = group.find(By.id("soy-toolbar-dropdown"));
        assertHasRootElementProperties(dropdown, "soy-toolbar-dropdown", null);
        
        PageElement dropdownTrigger = dropdown.find(By.className("toolbar-trigger"));
        assertTextEquals(dropdownTrigger, "AUI Toolbar Dropdown &amp;");
        dropdownTrigger.click();
        assertTextEquals(dropdown.find(By.className("dropdown-item")), "AUI Dropdown Item");

        //Split Button

        PageElement splitButton = group.find(By.id("soy-toolbar-split-button"));
        assertHasRootElementProperties(splitButton, "soy-toolbar-split-button", null);

        PageElement splitButtonTrigger = splitButton.find(By.cssSelector("#soy-toolbar-split-button > .toolbar-trigger"));
        assertTextEquals(splitButtonTrigger, "AUI Toolbar Split-Button &amp;");

        PageElement splitDropdownTrigger = splitButton.find(By.cssSelector("#soy-toolbar-split-button .aui-dd-parent .toolbar-trigger"));
        splitDropdownTrigger.click();
        assertTextEquals(splitButton.find(By.className("dropdown-item")), "AUI Dropdown Item");
    }

    private PageElement findFirstXContainingY(PageElementFinder finder, By x, By y) {
        for(PageElement thingToFind : finder.findAll(x)) {
            if (thingToFind.find(y).isPresent()) {
                return thingToFind;
            }
        }
        return null;
    }

    private PageElement findFieldsetForId(PageElementFinder finder, String id) {
        return findFirstXContainingY(finder, By.tagName("fieldset"), By.id(id));
    }

    private PageElement findFieldGroupForId(PageElementFinder finder, String id) {
        return findFirstXContainingY(finder, By.className("field-group"), By.id(id));
    }
    
    private PageElement findContainerForCheckboxOrRadio(PageElementFinder finder, String type, String id) {
        return findFirstXContainingY(finder, By.className(type), By.id(id));
    }
    
    private void assertField(PageElement fieldGroup, String id, String type, String rawText, String escapedText) {
        assertTrue("Field group should be found for #" + id, fieldGroup.isPresent());
        assertHasRootElementProperties(fieldGroup, null, null);
        
        PageElement label = fieldGroup.find(By.tagName("label"));
        assertAttributeEquals(label, "for", id);
        assertTrue("Label has required icon", label.find(By.className("icon-required")).isVisible());
        assertTextEquals(label, escapedText);
        
        PageElement input = fieldGroup.find(By.id(id));
        String tagName = input.getTagName();
        if ("input".equalsIgnoreCase(tagName)) {
            if (Arrays.asList("text", "password", "submit", "button").contains(type)) {
                assertAttributeEquals(input, "value", rawText);
            }
            assertAttributeEquals(input, "type", type);
        } else if ("textarea".equalsIgnoreCase(tagName) || "span".equalsIgnoreCase(tagName)) {
            assertTextEquals(input, rawText);
        } else if ("select".equalsIgnoreCase(tagName)) {
            assertTrue("Select should have the multiple attribute.", input.getAttribute("multiple") != null);
        }
        if (!"value".equals(type)) {
            assertAttributeEquals(input, "name", id + "-name");
            assertTrue("Input should have the disabled attribute.", input.getAttribute("disabled") != null);
        }
        assertHasClass(input, "password".equals(type) ? "text" :
                              "submit".equals(type) ? "button" :
                              "value".equals(type) ? "field-value" :
                              "select".equals(type) ? "multi-select" :
                              type);

        PageElement description = fieldGroup.find(By.className("description"));
        assertTextEquals(description, rawText);

        PageElement error = fieldGroup.find(By.className("error"));
        assertTextEquals(error, rawText);
    }
    
    private void assertCheckboxOrRadioField(PageElement fieldset, String id, String type, String rawText, String escapedText, String item1RawText, String item1EscapedText) {
        assertHasRootElementProperties(fieldset, id, null);
        assertHasClass(fieldset, "group");

        PageElement legend = fieldset.find(By.tagName("legend"));
        assertTrue("Legend has required icon", legend.find(By.className("icon-required")).isPresent());
        assertTextEquals(legend, escapedText);
        
        PageElement matrix = fieldset.find(By.className("matrix"));
        
        PageElement item1Container = findContainerForCheckboxOrRadio(fieldset, type, id + "-1");
        assertHasRootElementProperties(item1Container, null, null);
        assertHasClass(item1Container, type);
        
        PageElement item1 = item1Container.find(By.id(id + "-1"));
        assertHasClass(item1, type);
        assertAttributeEquals(item1, "name", "checkbox".equals(type) ? id + "-1-name" : id + "-name");
        assertAttributeEquals(item1, "checked", "true");

        PageElement item1Label = item1Container.find(By.tagName("label"));
        assertAttributeEquals(item1Label, "for", id + "-1");
        assertTextEquals(item1Label, item1EscapedText);

        PageElement item1Description = item1Container.find(By.className("description"));
        assertTextEquals(item1Description, item1RawText);

        PageElement item1Error = item1Container.find(By.className("error"));
        assertTextEquals(item1Error, item1RawText);

        PageElement errorsAndDescription = fieldset.find(By.className("field-group"));

        PageElement description = errorsAndDescription.find(By.className("description"));
        assertTextEquals(description, rawText);

        PageElement error = errorsAndDescription.find(By.className("error"));
        assertTextEquals(error, rawText);
    }

    @Test
    public void testComplexForm() {
        PageElement form = elementFinder.find(By.id("complex-form"));
        assertHasRootElementProperties(form, "complex-form", null);
        assertHashEquals(form, "action", "#form");
        //assertAttributeEquals(form, "method", "GET"); // fails but attribute is correct
        assertAttributeEquals(form, "enctype", "text/plain");
        assertHasClass(form, "unsectioned");
        assertHasClass(form, "long-label");
        assertHasClass(form, "top-label");
        
        PageElement description = form.find(By.id("complex-description"));
        assertHasRootElementProperties(description, "complex-description", null);
        assertTextEquals(description, "AUI Form &");

        PageElement fieldset = form.find(By.id("complex-fieldset"));
        assertHasRootElementProperties(fieldset, "complex-fieldset", null);
        assertHasClass(fieldset, "inline");
        
        PageElement textField = findFieldGroupForId(form, "complex-text-field");
        assertField(textField, "complex-text-field", "text", "Text Field &amp;", "Text Field &");

        PageElement textInput = textField.find(By.id("complex-text-field"));
        assertAttributeEquals(textInput, "maxlength", "16");
        assertAttributeEquals(textInput, "size", "18");
        
        PageElement textareaField = findFieldGroupForId(form, "complex-textarea-field");
        assertField(textareaField, "complex-textarea-field", "textarea", "Textarea Field &amp;", "Textarea Field &");

        PageElement textarea = textareaField.find(By.id("complex-textarea-field"));
        assertAttributeEquals(textarea, "rows", "2");
        assertAttributeEquals(textarea, "cols", "20");

        PageElement passwordField = findFieldGroupForId(form, "complex-password-field");
        assertField(passwordField, "complex-password-field", "password", "Password Field &amp;", "Password Field &");

        PageElement fileField = findFieldGroupForId(form, "complex-file-field");
        assertField(fileField, "complex-file-field", "file", "File Field &amp;", "File Field &");

        PageElement selectField = findFieldGroupForId(form, "complex-select-field");
        assertField(selectField, "complex-select-field", "select", "Select Field &amp;", "Select Field &");

        PageElement select = selectField.find(By.id("complex-select-field"));
        assertAttributeEquals(select, "size", "3");
        
        PageElement checkboxFieldset = findFieldsetForId(fieldset, "complex-checkbox-field");
        assertCheckboxOrRadioField(checkboxFieldset, "complex-checkbox-field", "checkbox", "Checkbox Field &amp;", "Checkbox Field &", "Checkbox 1 &amp;", "Checkbox 1 &");

        PageElement radioFieldset = findFieldsetForId(fieldset, "complex-radio-field");
        assertCheckboxOrRadioField(radioFieldset, "complex-radio-field", "radio", "Radio Field &amp;", "Radio Field &", "Radio 1 &amp;", "Radio 1 &");

        PageElement valueField = findFieldGroupForId(form, "complex-value-field");
        assertField(valueField, "complex-value-field", "value", "Value Field &amp;", "Value Field &");
        
    }
    
    public void assertComplexAuiMessage(String type) {
        PageElement complexMessage = elementFinder.find(By.id("soy-message-" + type));
        assertHasRootElementProperties(complexMessage, "soy-message-" + type, "section");

        PageElement messageTitle = elementFinder.find(By.cssSelector("#soy-message-" + type + " .title"));
        assertTextEquals(messageTitle, "AUI Message (" + type + ") &amp;");

        PageElement messageContents = elementFinder.find(By.id("soy-message-" + type + "-content"));
        assertTextEquals(messageContents, "AUI Message (" + type + ") &");
    }

    @Test
    public void testComplexMessages() {
        assertComplexAuiMessage("success");
        assertComplexAuiMessage("error");
        assertComplexAuiMessage("hint");
        assertComplexAuiMessage("info");
        assertComplexAuiMessage("warning");
        assertComplexAuiMessage("generic");
    }
}
