package it.com.atlassian.aui.javascript.integrationTests.experimental;

import com.atlassian.pageobjects.aui.component.restfultable.EditRow;
import com.atlassian.pageobjects.aui.component.restfultable.RestfulTable;
import com.atlassian.pageobjects.aui.component.restfultable.Row;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.RestfulTablePage;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class AUIRestfultableTest extends AbstractAuiIntegrationTest
{

    RestfulTablePage restfulTablePage;

    @Before
    public void setup()
    {
        restfulTablePage = product.visit(RefappLoginPage.class).loginAsSysAdmin(RestfulTablePage.class);
    }

    @Test
    public void testSuccessfullyCreatingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        assertEquals("Scott", table.getFirstRow().getValue("name"));
        assertEquals("Work", table.getFirstRow().getValue("group"));
        assertEquals("0412947430", table.getFirstRow().getValue("number"));
    }

    @Test
    public void testValidationErrorCreatingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        final EditRow createRow = table.getCreateRow();
        createRow.fill("name", "Scott", "group", "Work", "number", "invalidno")
                .submit();
        assertTrue(createRow.getValidationErrors().containsValue("Not a valid number"));
    }

    @Test
    public void testDeletingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        final Row firstRow = table.getFirstRow();
        firstRow.delete();
        assertTrue(table.isEmpty());
    }

    @Test
    public void testEditingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Row firstRow = table.getFirstRow();
        firstRow.edit("group").setFieldValue("group", "Family").submit();
        assertEquals("Family", firstRow.getValue("group"));
    }

    @Test
    public void testValidationErrorEditingEntry()
    {
        RestfulTable table = restfulTablePage.getTable();
        table.addEntry("name", "Scott", "group", "Work", "number", "0412947430");
        Row firstRow = table.getFirstRow();
        EditRow editRow = firstRow.edit("number").setFieldValue("number", "dgasdgsd").submit();
        assertTrue(editRow.getValidationErrors().containsValue("Not a valid number"));
    }
}
