package it.com.atlassian.aui.javascript.integrationTests;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import it.com.atlassian.aui.javascript.AbstractAuiIntegrationTest;
import it.com.atlassian.aui.javascript.pages.MessagesTestPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Selenium test for events.js
 */
public class AUIMessagesTest extends AbstractAuiIntegrationTest
{

    private MessagesTestPage messagesTestPage;
    private PageElementFinder elementFinder;

    @Before
    public void setup()
    {
        messagesTestPage = product.visit(MessagesTestPage.class);
        elementFinder = messagesTestPage.getElementFinder();
    }

    //Make sure javascript messages work when no context is supplied
    @Test
    public void testJavascriptMessagesWithNoContext()
    {
        PageElement successMessage = elementFinder.find(By.cssSelector("#aui-message-bar .success"));
        PageElement infoMessage = elementFinder.find(By.cssSelector("#aui-message-bar .info"));

        assertTrue(successMessage.isPresent());
        assertTrue(infoMessage.isPresent());
    }

    //Make sure javascript messsages work when a context is supplied
    @Test
    public void testJavscriptMessagesWithContext()
    {
        PageElement warningMessage = elementFinder.find(By.cssSelector("#context .warning"));
        PageElement hintMessage = elementFinder.find(By.cssSelector("#context .hint"));

        assertTrue(warningMessage.isPresent());
        assertTrue(hintMessage.isPresent());
    }

    //Make sure closeable HTML messages close correctly
    @Test
    public void testHTMLCloseable()
    {
        PageElement closeableMessage = elementFinder.find(By.cssSelector("#closeable-html-test .closeable"));
        PageElement closeIcon = closeableMessage.find(By.cssSelector(".aui-icon.icon-close"));

        assertTrue("Closeable message should be present", closeableMessage.isPresent());
        closeIcon.click();
        assertFalse("Closeable message should not be present", closeableMessage.isPresent());
    }

    @Test
    public void testJSCloseable()
    {
        PageElement closeableMessage = elementFinder.find(By.cssSelector("#closeable-js-test .closeable"));
        PageElement closeIcon = closeableMessage.find(By.cssSelector(".aui-icon.icon-close"));
        assertTrue(closeableMessage.isPresent());
        closeIcon.click();
        assertFalse(closeableMessage.isPresent());
    }

    @Test
    // Obviously wouldn't work in IE8, but webdriver ignorebrowser is b0rken
    public void testMessagePrepend()
    {
        PageElement prependMessage = elementFinder.find(By.cssSelector("#insert-before"));
        assertTrue(prependMessage.isPresent());
        PageElement appendMessage = elementFinder.find(By.cssSelector("#insert-after"));
        assertTrue(appendMessage.isPresent());
        PageElement firstElement = elementFinder.find(By.cssSelector("#aui-insert-option-bar div:first-of-type"));
        assertTrue(firstElement.hasClass("info"));
        PageElement lastElement = elementFinder.find(By.cssSelector("#aui-insert-option-bar div:last-of-type"));
        assertTrue(lastElement.hasClass("warning"));
    }
}
