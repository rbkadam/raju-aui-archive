AJS.$(function() {

    var $container = AJS.$("#soy-test");

    var $simpleContainer = AJS.$('<section id="simple-calls"><h3>Simple (only required attributes specified)</h3></section>').appendTo($container);

    $simpleContainer
        .append(atlassian.panel({
            content : '<div>AUI Panel</div>'
        }))
        .append(atlassian.group.group({
            content : atlassian.group.item({ content : '<div>AUI Item in AUI Group</div>', id:'soy-item'})
        }))
        .append(atlassian.table({
            content : '<tr><td>AUI Table</td></tr>'
        }))
        .append(atlassian.tabs({
            menuItems : [{
                isActive : true,
                url : '#tab1',
                text : 'Tab 1'
            }, {
                url : '#tab2',
                text : 'Tab 2'
            }],
            paneContent : atlassian.tabPane({
                isActive : true,
                content : 'Tab 1 Content'
            }) + atlassian.tabPane({
                content : 'Tab 2 Content'
            })
        }))
        .append(atlassian.dropdown.parent({
            content : atlassian.dropdown.trigger({
                accessibilityText : "AUI Dropdown Trigger"
            }) + atlassian.dropdown.menu({
                content : atlassian.dropdown.item({
                    text : 'AUI Dropdown Item'
                })
            })
        }))
        .append(atlassian.toolbar.toolbar({
            content : atlassian.toolbar.split({
                split : 'left',
                content : atlassian.toolbar.group({
                    content : atlassian.toolbar.button({
                        text : 'AUI Button (text)'
                    }) + atlassian.toolbar.button({
                        title : 'AUI Button (icon)',
                        iconClass : 'my-toolbar-icon'
                    }) + atlassian.toolbar.link({
                        url : '#link',
                        text : 'AUI Link'
                    }) + atlassian.toolbar.dropdown({
                        text : 'AUI Toolbar Dropdown',
                        dropdownItemsContent : atlassian.dropdown.item({ text : 'AUI Dropdown Item' })
                    }) + atlassian.toolbar.splitButton({
                        text : 'AUI Toolbar Split-Button',
                        dropdownItemsContent : atlassian.dropdown.item({ text : 'AUI Dropdown Item' })
                    })
                })
            })
        }))
        .append(atlassian.form.form({
            action : '#form',
            content : atlassian.form.formDescription({
                content : 'AUI Form &amp;'
            }) + atlassian.form.fieldset({
                legendContent : 'Fieldset &amp;',
                content : atlassian.form.textField({
                    id : 'simple-text-field',
                    labelContent : 'Text Field &amp;'
                }) + atlassian.form.textareaField({
                    id : 'simple-textarea-field',
                    labelContent : 'Textarea Field &amp;'
                }) + atlassian.form.passwordField({
                    id : 'simple-password-field',
                    labelContent : 'Password Field &amp;'
                }) + atlassian.form.fileField({
                    id : 'simple-file-field',
                    labelContent : 'File Field &amp;'
                }) + atlassian.form.selectField({
                    id : 'simple-select-field',
                    labelContent : 'Select Field &amp;',
                    options : [{
                        text : 'Group 1 &amp;',
                        options : [{
                            text : 'Option 1.1 &amp;',
                            value : '1.1 &amp;'
                        }, {
                            text : 'Option 1.2 &amp;',
                            value : '1.2 &amp;'
                        }]
                    }, {
                        text : 'Option 2 &amp;',
                        value : '2 &amp;'
                    }]
                }) + atlassian.form.checkboxField({
                    legendContent : 'Checkbox Field &amp;',
                    fields : [{
                        id : 'simple-checkbox-field-1',
                        labelText : 'Checkbox 1 &'
                    }, {
                        id : 'simple-checkbox-field-2',
                        labelText : 'Checkbox 2 &'
                    }, {
                        id : 'simple-checkbox-field-3',
                        labelText : 'Checkbox 3 &'
                    }]
                }) + atlassian.form.radioField({
                    name : 'simple-radio-field',
                    legendContent : 'Radio Field &amp;',
                    fields : [{
                        id : 'simple-radio-field-1',
                        value : 'radio-1',
                        labelText : 'Radio 1 &'
                    }, {
                        id : 'simple-radio-field-2',
                        value : 'radio-2',
                        labelText : 'Radio 2 &'
                    }, {
                        id : 'simple-radio-field-3',
                        value : 'radio-3',
                        labelText : 'Radio 3 &'
                    }]
                }) + atlassian.form.valueField({
                    id : 'simple-value-field',
                    labelContent : 'Value Field &amp;',
                    value : 'Value Field &amp;'
                })
            }) + atlassian.form.buttons({
                content : atlassian.form.submit({
                    name : 'submit',
                    text : 'Submit &amp;'
                }) + atlassian.form.button({
                    name : 'button',
                    text : 'Button &amp;'
                }) + atlassian.form.linkButton({
                    name : 'link-button',
                    text : 'Link-Button &amp;'
                })
            })
        }));

    function appendSimpleAuiMessage(type) {
        return this.append(atlassian.message[type]({
            content : '<p>AUI Message (' + type + ')</p>'
        }));
    }
    $simpleContainer.appendAuiMessage = appendSimpleAuiMessage;
    $simpleContainer
        .appendAuiMessage('success')
        .appendAuiMessage('error')
        .appendAuiMessage('hint')
        .appendAuiMessage('info')
        .appendAuiMessage('warning')
        .appendAuiMessage('generic');



    var $complexContainer = AJS.$('<section id="complex-calls"><h3>Complex (all attributes specified)</h3></section>').appendTo($container);

    $complexContainer
        .append(atlassian.panel({
            id : 'soy-panel',
            content: '<div>AUI Panel &amp;</div>',
            extraClasses : 'extra-class',
            extraAttributes : { "data-attr" : "extra-attr" },
            tagName : 'section'
        }))
        .append(atlassian.group.group({
            isSplit : true,
            id : 'soy-group',
            content: atlassian.group.item({
                content : '<div>AUI Item 1 in AUI Group &amp;</div>',
                id:'soy-item-1',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" },
                tagName : 'section'
            }) + atlassian.group.item({
                content : '<div>AUI Item 2 in AUI Group &amp;</div>',
                id:'soy-item-2',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" },
                tagName : 'section'
            }),
            extraClasses : 'extra-class',
            extraAttributes : { "data-attr" : "extra-attr" },
            tagName : 'section'
        }))
        .append(atlassian.table({
            id : 'soy-table',
            contentIncludesTbody : true,
            content: '<tbody><tr><td>AUI Table (tbody) &amp;</td></tr></tbody>',
            columnsContent : '<colgroup><col id="complex-table-column"></colgroup>',
            theadContent: '<tr><td>AUI Table (thead) &amp;</td></tr>',
            tfootContent: '<tr><td>AUI Table (tfoot) &amp;</td></tr>',
            captionContent: 'AUI Table (caption) &amp;',
            extraClasses : 'extra-class',
            extraAttributes : { "data-attr" : "extra-attr" }
        }))
        .append(atlassian.tabs({
            isVertical : true,
            isDisabled : true,
            id:'soy-tabs',
            extraClasses : 'extra-class',
            extraAttributes : { "data-attr" : "extra-attr" },
            tagName : 'section',
            menuItems : [{
                isActive : true,
                url : '#soy-tab-pane-1',
                text : 'Tab 1 &amp;',
                id:'soy-tab-menu-1',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" }
            }, {
                isActive : false,
                url : '#soy-tab-pane-2',
                text : 'Tab 2',
                id:'soy-tab-menu-2',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" }
            }],
            paneContent : atlassian.tabPane({
                isActive : true,
                content : 'Tab 1 Content - Disabled tabs &amp;',
                id:'soy-tab-pane-1',
                extraClasses : ['extra-class', 'extra-class-2'],
                extraAttributes : 'data-attr="extra-attr" data-attr-2="extra-attr-2"',
                tagName : 'section'
            }) + atlassian.tabPane({
                isActive : false,
                content : 'Tab 2 Content',
                id:'soy-tab-pane-2',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" },
                tagName : 'section'
            })
        }))
        .append(atlassian.dropdown.parent({
            id: 'soy-dropdown-parent',
            extraClasses : 'extra-class',
            extraAttributes : { "data-attr" : "extra-attr" },
            tagName : 'section',
            content : atlassian.dropdown.trigger({
                id: 'soy-dropdown-trigger',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" },
                tagName : 'section',
                accessibilityText : "AUI Dropdown Trigger &amp;",
                showIcon : false
            }) + atlassian.dropdown.menu({
                id: 'soy-dropdown-menu',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" },
                tagName : 'section',
                content : atlassian.dropdown.item({
                    id: 'soy-dropdown-item',
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" },
                    tagName : 'section',
                    text : 'AUI Dropdown Item &amp;',
                    url : '#dropdown-item'
                })
            })
        }))
        .append(atlassian.toolbar.toolbar({
            id: 'soy-toolbar',
            extraClasses : 'extra-class',
            extraAttributes : { "data-attr" : "extra-attr" },
            tagName : 'section',
            content : atlassian.toolbar.split({
                id: 'soy-toolbar-split-left',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" },
                tagName : 'section',
                split : 'left',
                content : atlassian.toolbar.group({
                    id: 'soy-toolbar-group',
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" },
                    content : atlassian.toolbar.button({
                        id: 'soy-toolbar-button-1',
                        extraClasses : 'extra-class',
                        extraAttributes : { "data-attr" : "extra-attr" },
                        text : 'AUI Button (text) &amp;',
                        url : '#toolbar-button',
                        isPrimary : true,
                        isActive : true
                    }) + atlassian.toolbar.button({
                        id: 'soy-toolbar-button-2',
                        extraClasses : 'extra-class',
                        extraAttributes : { "data-attr" : "extra-attr" },
                        title : 'AUI Button (icon) &amp;',
                        iconClass : 'my-toolbar-icon',
                        url : '#toolbar-button',
                        isPrimary : true,
                        isActive : true
                    }) + atlassian.toolbar.link({
                        id: 'soy-toolbar-link',
                        extraClasses : 'extra-class',
                        extraAttributes : { "data-attr" : "extra-attr" },
                        url : '#link',
                        text : 'AUI Link &amp;'
                    }) + atlassian.toolbar.dropdown({
                        id: 'soy-toolbar-dropdown',
                        extraClasses : 'extra-class',
                        extraAttributes : { "data-attr" : "extra-attr" },
                        text : 'AUI Toolbar Dropdown &amp;',
                        isPrimary : true,
                        dropdownItemsContent : atlassian.dropdown.item({ text : 'AUI Dropdown Item' })
                    }) + atlassian.toolbar.splitButton({
                        id: 'soy-toolbar-split-button',
                        extraClasses : 'extra-class',
                        extraAttributes : { "data-attr" : "extra-attr" },
                        text : 'AUI Toolbar Split-Button &amp;',
                        url : '#split-button',
                        isPrimary : true,
                        dropdownItemsContent : atlassian.dropdown.item({ text : 'AUI Dropdown Item' })
                    })
                })
            })
        }))
        .append(atlassian.form.form({
            id : 'complex-form',
            extraClasses : 'extra-class',
            extraAttributes : { "data-attr" : "extra-attr" },
            action : '#form',
            method : 'GET',
            enctype : 'text/plain',
            isUnsectioned : true,
            isLongLabels : true,
            isTopLabels : true,
            content : atlassian.form.formDescription({
                id : 'complex-description',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" },
                content : 'AUI Form &amp;'
            }) + atlassian.form.fieldset({
                id : 'complex-fieldset',
                extraClasses : 'extra-class',
                extraAttributes : { "data-attr" : "extra-attr" },
                isInline : true,
                legendContent : 'Fieldset &amp;',
                content : atlassian.form.textField({
                    id : 'complex-text-field',
                    name : 'complex-text-field-name',
                    labelContent : 'Text Field &amp;',
                    value : 'Text Field &amp;',
                    maxLength : 16,
                    size : 18,
                    isRequired : true,
                    isDisabled : true,
                    descriptionText : 'Text Field &amp;',
                    errorTexts : [ 'Text Field &amp;' ],
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.textareaField({
                    id : 'complex-textarea-field',
                    name : 'complex-textarea-field-name',
                    labelContent : 'Textarea Field &amp;',
                    value : 'Textarea Field &amp;',
                    rows : 2,
                    cols : 20,
                    isRequired : true,
                    isDisabled : true,
                    descriptionText : 'Textarea Field &amp;',
                    errorTexts : [ 'Textarea Field &amp;' ],
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.passwordField({
                    id : 'complex-password-field',
                    name : 'complex-password-field-name',
                    labelContent : 'Password Field &amp;',
                    value : 'Password Field &amp;',
                    isRequired : true,
                    isDisabled : true,
                    descriptionText : 'Password Field &amp;',
                    errorTexts : [ 'Password Field &amp;' ],
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.fileField({
                    id : 'complex-file-field',
                    name : 'complex-file-field-name',
                    labelContent : 'File Field &amp;',
                    value : 'File Field &amp;',
                    isRequired : true,
                    isDisabled : true,
                    descriptionText : 'File Field &amp;',
                    errorTexts : [ 'File Field &amp;' ],
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.selectField({
                    id : 'complex-select-field',
                    name : 'complex-select-field-name',
                    labelContent : 'Select Field &amp;',
                    options : [{
                        text : 'Group 1 &amp;',
                        options : [{
                            text : 'Option 1.1 &amp;',
                            value : '1.1 &amp;'
                        }, {
                            text : 'Option 1.2 &amp;',
                            value : '1.2 &amp;'
                        }]
                    }, {
                        text : 'Option 2 &amp;',
                        value : '2 &amp;'
                    }],
                    isMultiple : true,
                    size : 3,
                    isRequired : true,
                    isDisabled : true,
                    descriptionText : 'Select Field &amp;',
                    errorTexts : [ 'Select Field &amp;' ],
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.checkboxField({
                    id : 'complex-checkbox-field',
                    legendContent : 'Checkbox Field &amp;',
                    fields : [{
                        id : 'complex-checkbox-field-1',
                        name : 'complex-checkbox-field-1-name',
                        labelText : 'Checkbox 1 &',
                        isChecked : true,
                        isDisabled : true,
                        descriptionText : 'Checkbox 1 &amp;',
                        errorTexts : [ 'Checkbox 1 &amp;' ],
                        extraClasses : 'extra-class',
                        extraAttributes : { "data-attr" : "extra-attr" }
                    }, {
                        id : 'complex-checkbox-field-2',
                        name : 'complex-checkbox-field-2-name',
                        labelText : 'Checkbox 2 &'
                    }, {
                        id : 'complex-checkbox-field-3',
                        name : 'complex-checkbox-field-3-name',
                        labelText : 'Checkbox 3 &'
                    }],
                    isRequired : true,
                    isMatrix : true,
                    descriptionText : 'Checkbox Field &amp;',
                    errorTexts : [ 'Checkbox Field &amp;' ],
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.radioField({
                    id : 'complex-radio-field',
                    name : 'complex-radio-field-name',
                    legendContent : 'Radio Field &amp;',
                    fields : [{
                        id : 'complex-radio-field-1',
                        value : 'radio-1',
                        labelText : 'Radio 1 &',
                        isChecked : true,
                        isDisabled : true,
                        descriptionText : 'Radio 1 &amp;',
                        errorTexts : [ 'Radio 1 &amp;' ],
                        extraClasses : 'extra-class',
                        extraAttributes : { "data-attr" : "extra-attr" }
                    }, {
                        id : 'complex-radio-field-2',
                        value : 'radio-2',
                        labelText : 'Radio 2 &'
                    }, {
                        id : 'complex-radio-field-3',
                        value : 'radio-3',
                        labelText : 'Radio 3 &'
                    }],
                    isRequired : true,
                    isMatrix : true,
                    descriptionText : 'Radio Field &amp;',
                    errorTexts : [ 'Radio Field &amp;' ],
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.valueField({
                    id : 'complex-value-field',
                    labelContent : 'Value Field &amp;',
                    value : 'Value Field &amp;',
                    isRequired : true,
                    descriptionText : 'Value Field &amp;',
                    errorTexts : [ 'Value Field &amp;' ],
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                })
            }) + atlassian.form.buttons({
                alignment : 'right',
                content : atlassian.form.submit({
                    id : 'complex-form-submit',
                    name : 'submit',
                    text : 'Submit &amp;',
                    isDisabled : true,
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.button({
                    id : 'complex-form-button',
                    name : 'button',
                    text : 'Button &amp;',
                    isDisabled : true,
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                }) + atlassian.form.linkButton({
                    id : 'complex-form-link-button',
                    name : 'link-button',
                    text : 'Link-Button &amp;',
                    url : '#link-button',
                    extraClasses : 'extra-class',
                    extraAttributes : { "data-attr" : "extra-attr" }
                })
            })
        }));

    function appendComplexAuiMessage(type) {
        return this.append(atlassian.message[type]({
            id : 'soy-message-' + type,
            content: '<p id="soy-message-' + type + '-content">AUI Message (' + type + ') &amp;</p>',
            title : 'AUI Message (' + type + ') &amp;',
            extraClasses : 'extra-class',
            extraAttributes : { "data-attr" : "extra-attr" },
            tagName : 'section',
            isCloseable : true,
            isShadowed : true
        }));
    }
    $complexContainer.appendAuiMessage = appendComplexAuiMessage;
    $complexContainer
        .appendAuiMessage('success')
        .appendAuiMessage('error')
        .appendAuiMessage('hint')
        .appendAuiMessage('info')
        .appendAuiMessage('warning')
        .appendAuiMessage('generic');




    AJS.dropDown.Standard({
        useLive : true
    });
});