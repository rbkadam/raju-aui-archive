/**
 * Common JS for test pages only.
 * Do not load resources for test pages - they must be viewed in the refapp.
 */
(function () {

    window.onload = function() {
        if (window.location.protocol == 'file:') {
            //var header = document.getElementById("bootcamp-header") || document.getElementById("test-header");
            var header = document.getElementById("bootcamp-header") || document.getElementsByTagName("body")[0];
            header.innerHTML = 'Error: this page must be viewed via the refapp: <a href="http://localhost:9999/ajs/">http://localhost:9999/ajs</a>';
            header.style.color = "red";
        }
    };
   
})();
 
AUITEST = AUITEST || {};

var AUITEST = {

    // DRY, innit.
    newTestDropdown: function (id,ddType) {
        var base = jQuery('<div class="aui-dropdown2 aui-style-default"><ul><li><a href="#">Attach File</a></li><li><a href="#">Comment</a></li><li><a href="#">Edit Issue</a></li><li><a href="#">Watch Issue</a></li></ul></div>'),
            type = ddType || 'generic';
            
        if ( type == "unstyled") {
            base.removeClass("aui-style-default");
        }
        if ( type == "forcedwidth") {
            base.addClass("test-forced-width");
        }
        
        base.attr("id",id).appendTo("body");
        return base;
    }

};

