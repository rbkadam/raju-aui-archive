(function ($) {

    var EditGroupView = AJS.RestfulTable.CustomEditView.extend({
        render: function (self) {
            var $select = $("<select name='group'>" +
                    "<option value='Friends'>Friends</option>" +
                    "<option value='Family'>Family</option>" +
                    "<option value='Work'>Work</option>" +
                    "</select>");

            $select.val(self.value); // select currently selected
            return $select;
        }
    });

    var NameReadView = AJS.RestfulTable.CustomReadView.extend({
        render: function (self) {
            return $("<strong />").text(self.value);
        }
    });

    // DOM ready
    $(function () {

        var url = AJS.contextPath() + "/rest/contacts/1.0/contacts",
            $contactsTable = $("#contacts-table");

        new AJS.RestfulTable({
            el: $contactsTable, // <table>
            autofocus: true, // auto focus first field of create row
            columns: [
                {id: "name", header: "Name", readView: NameReadView}, // id is the mapping of the rest property to render
                {id: "group", header: "Group", editView: EditGroupView}, // header is the text in the <th>
                {id: "number", header: "Number"}
            ],
            resources: {
                all: url, // resource to get all contacts
                self: url // resource to get single contact url/{id}
            },
            noEntriesMsg: "You have no contacts. Loner!", // message to be displayed when table is empty
            allowReorder: true // drag and drop reordering
        });
    });

})(AJS.$);