# AUI

AUI is the Atlassian User Interface library. It is open source; and provided both as a plugin for use in Atlassian products and a static file 'flat pack'. Note the flat pack does not contain those AUI components which rely on backend functionality.

## Documentation
* [Component documentation](http://developer.atlassian.com/display/AUI/)
* [Demo Pages](http://docs.atlassian.com/aui/latest/demo-pages/)
* [Sandbox Tool](http://docs.atlassian.com/aui/latest/sandbox/)
* [Release Notes](https://developer.atlassian.com/display/AUI/AUI+Release+Notes)

## Quick Start Guide - Static files

If you want to do quick prototypes or deploy AUI using static files, [see the latest release notes for a 'flat pack' download](https://developer.atlassian.com/display/AUI/AUI+Release+Notes). You can simply unzip the flat pack and get started.

## Quick Start Guide - Atlassian Plugin
The AUI build uses the Atlassian reference application (REFAPP) for development and testing. The simplest way to ensure you have the dependencies covered is to [install the Atlassian Plugin SDK](https://developer.atlassian.com/display/DOCS/Developing+with+the+Atlassian+Plugin+SDK). Once you have the SDK installed...

* Clone or fork AUI
* Open the root directory
* execute: `mvn clean install -Dmaven.test.skip` (or `mvn clean install` if you want to double check for a green baseline)
* Go to `/auiplugin-tests/`
* execute: `mvn refapp:run`
* you should now be able to view the refapp at [http://localhost:9999/ajs/](http://localhost:9999/ajs/).

See the [AUI Contributor Guide](https://developer.atlassian.com/display/AUI/AUI+Contributor+Guide) for more details.

## Tests
Run the tests by executing `mvn clean install`. This will install a browser and run both the Unit and Integration tests.
