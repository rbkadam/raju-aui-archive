package com.atlassian.pageobjects.aui.component.restfultable;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.AtlassianWebDriver;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class RestfulTable
{

    private PageElement table;
    private EditRow createRow;
    private final String id;

    @Inject
    private AtlassianWebDriver driver;

    @Inject
    private PageBinder binder;

    @Inject
    private PageElementFinder finder;

    public RestfulTable(final String id)
    {
        this.id = id;
    }

    @Init
    private void getElements()
    {
        table = finder.find(By.id(id));
        createRow = binder.bind(EditRow.class,
                table.find(By.cssSelector(".aui-restfultable-create .aui-restfultable-row")));
    }

    public RestfulTable addEntry(final String... fields)
    {
        createRow.fill(fields).submit();
        return this;
    }

    public EditRow getCreateRow()
    {
        return createRow;
    }

    public Row getFirstRow()
    {
        return binder.bind(Row.class, table.find(By.cssSelector(".aui-restfultable-readonly:first-of-type")));
    }

    public boolean isEmpty()
    {
        return table.find(By.className("aui-restfultable-no-entires")).isPresent();
    }
}
