package com.atlassian.pageobjects.aui.component.restfultable;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

public class EditRow
{
    private final PageElement row;

    public EditRow(final PageElement row)
    {
        this.row = row;
    }

    @Inject
    private PageElementFinder finder;

    public EditRow fill(final String... fields)
    {

        for (int i=0; i < fields.length; i=i+2)
        {
            row.find(By.name(fields[i])).type(fields[i+1]);
        }

        return this;
    }

    public EditRow setFieldValue(final String field, final String value) {
        row.find(By.name(field)).type(value);
        return this;
    }

    public Map<String, String> getValidationErrors()
    {
        final List<PageElement> errorEls = row.findAll(By.className("error"));
        Map<String, String> errors = new HashMap<String, String>();
        for (PageElement errorEl : errorEls)
        {
            errors.put(errorEl.getAttribute("data-field"), errorEl.getText());
        }
        return errors;
    }

    public EditRow submit()
    {
        row.find(By.className("aui-button")).click();
        waitUntilFalse(row.timed().hasClass("loading"));
        return this;
    }
}
